package com.example.devpedrorjas.basicacitivitynightmode;

import android.content.Context;
import android.content.SharedPreferences;

public class NightMode {
    private SharedPreferences sharedPreferencesNightMode;
    private String TAG_NightMode = "NIGHTMODE";

    NightMode(Context context) {
        sharedPreferencesNightMode = context.getSharedPreferences(TAG_NightMode, Context.MODE_PRIVATE);
    }

    void setNightMode(boolean nightMode) {
        SharedPreferences.Editor editor = sharedPreferencesNightMode.edit();
        editor.putBoolean(TAG_NightMode, nightMode).apply();
    }

    boolean loadNightMode() {
        return sharedPreferencesNightMode.getBoolean(TAG_NightMode, false);
    }
}
